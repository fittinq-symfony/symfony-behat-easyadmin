<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Service;

use Symfony\Component\Finder\Finder;

class CrudControllerService
{
    private array $crudControllers = [];

    public function __construct()
    {
        $this->setupCrudControllers();
    }

    private function setupCrudControllers(): void
    {
        $crudControllers = Finder::create()->files()->in("src/Controller/*")->name('*CrudController.php');

        foreach ($crudControllers as $controller) {
            $fullyQualifiedName = str_replace(["src", "/", ".php"], ["App", "\\", ""], $controller->getPathname());
            $className = basename(str_replace('\\', '/', $fullyQualifiedName));
            $className = str_replace('CrudController', '', $className);
            $this->crudControllers[$className] = $fullyQualifiedName;
        }
    }

    public function getCrudControllerNamespaceByClassname(string $className): string
    {
        return $this->crudControllers[$className];
    }
}