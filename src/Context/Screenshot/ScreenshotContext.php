<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Screenshot;

use Behat\MinkExtension\Context\RawMinkContext;

class ScreenshotContext extends RawMinkContext
{
    private int $counter = 1;

    /**
     * @Given /^takes a screenshot$/
     */
    public function takesAScreenshot()
    {
        file_put_contents("screenshot".$this->counter++.".png", $this->getSession()->getScreenshot());
    }
}