<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Field;

use Behat\MinkExtension\Context\RawMinkContext;

class OptionFieldContext extends RawMinkContext
{
    /**
     * @Given /^.*picks the ([^"]*) options? where field is (.*)$/
     */
    public function pickTheValuesFromTheAvailableOptions(string $values, string $fieldName)
    {
        $values = explode(', ', $values);
        $label = $this->getSession()->getPage()->find('xpath', "//label[text() = '$fieldName']");
        $parent = $label->getParent();

        foreach ($values as $key => $value) {
            if ($key === 0) {
                $label->click();

                    $parent->find('xpath', "//div[contains(text(), '$value')]")?->click() ??
                    $parent->find('xpath', "//span[contains(text(), '$value')]")?->click();
            }
            if ($key > 0) {
                    $parent->find('xpath', "//div[contains(text(), '$value')]")?->click() ??
                    $parent->find('xpath', "//span[contains(text(), '$value')]")?->click();
            }
        }
    }

    /**
     * @Given /^removes the ([^"]*) option where field is .*$/
     */
    public function removeOptionFromInputField(string $name)
    {
        $this->getSession()->getPage()
            ->find('xpath', "//div[contains(text(), '$name')]")
            ->find('xpath', "//a[contains(@class, 'remove')]")
            ->click();
    }

    /**
     * @Then /^I click outside the (.+)$/
     */
    public function iClickOutsideTheField(): void
    {
        $page = $this->getSession()->getPage();
        $page->find('css', 'body')->click();
    }
}