<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Field;

use Behat\MinkExtension\Context\RawMinkContext;

class DateFieldContext extends RawMinkContext
{
    /**
     * @Given /^.*set date time field (.*) to (.*)$/
     */
    public function theUserSetsTheDateTimeToValue(string $fieldName, string $dateTime)
    {
        $label = $this->getSession()->getPage()->find('xpath', "//label[text() = '$fieldName']");
        $label->click();

        $input = $label->getParent()->find('xpath', "//input");
        $input->setValue(null);
        $input->setValue($dateTime);
    }
}