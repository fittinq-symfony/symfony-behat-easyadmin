<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Field;

use Behat\MinkExtension\Context\RawMinkContext;

class TextFieldContext extends RawMinkContext
{
    /**
     * @Given /^set text field (.*) to value (.*)$/
     */
    public function setValueToField(string $fieldName, string $value)
    {
        $parent = $this->getSession()->getPage()->find('xpath', "//label[text() = '$fieldName']")->getParent();
        $parent->find('xpath', "//input")->setValue($value);
    }
}