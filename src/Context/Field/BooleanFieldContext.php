<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Field;

use Behat\MinkExtension\Context\RawMinkContext;

class BooleanFieldContext extends RawMinkContext
{
    /**
     * @Given /^click boolean field (.*)$/
     */
    public function clickBooleanField(string $fieldName)
    {
        $this->getSession()->getPage()->find('xpath', "//label[text() = '$fieldName']")->click();
    }
}