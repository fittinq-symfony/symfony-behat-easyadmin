<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Crud;

class CrudDeleteContext extends CrudContext
{
    /**
     * @When /^the user deletes entity with field (.*) and value (.*)$/
     */
    public function deleteObject(string $fieldName, string $valueName): void
    {
        $page = $this->getSession()->getPage();

        // Ensure that the page has loaded the table
        $this->ensureActionButtonsOnPage();

        // Find the row based on the field and value
        $row = $page->find('xpath', "//td[contains(@data-label, '$fieldName')]/span[contains(text(), '$valueName')]");

        if (empty($row)) {
            // Try to find the field as an anchor if it's not found as a span
            $row = $page->find('xpath', "//td[contains(@data-label, '$fieldName')]/a[contains(text(), '$valueName')]");
        }

        if (empty($row)) {
            throw new \Exception("Row with $fieldName and value $valueName not found.");
        }

        // Traverse up to the parent row to find the actions column (actions are typically in the last <td>)
        $parentRow = $row->getParent()->getParent();

        // Find and click the dropdown or delete button
        $deleteButton = $parentRow->find('css', 'a.action-delete');

        if (!$deleteButton) {
            throw new \Exception("Delete button not found for entity with $fieldName = $valueName.");
        }

        // Click the delete button (it triggers the modal)
        $deleteButton->click();

        // Wait for the modal to appear and confirm the deletion by clicking the modal's delete button
        $this->confirmDelete();
    }

    /**
     * This method ensures that the action buttons are visible.
     */
    private function ensureActionButtonsOnPage(): void
    {
        // Ensure the actions dropdown is visible
        $scrollIntoView = <<<JS
        (function() {
            var element = document.querySelector(".actions");
            if (element) {
                element.scrollIntoView();
            }
        })();
        JS;

        $this->getSession()->executeScript($scrollIntoView);

        // Wait for the page to fully load the actions
        $this->getSession()->getPage()->waitFor(5000, function () {
            return $this->getSession()->getPage()->find('css', '.actions') !== null;
        });
    }

    private function confirmDelete(): void
    {
        $page = $this->getSession()->getPage();

        // Wait for the modal to appear
        $modal = $page->find('css', '#modal-delete');
        if (!$modal) {
            throw new \Exception("Delete confirmation modal not found.");
        }

        // Click the delete button within the modal
        $confirmButton = $modal->find('css', '#modal-delete-button');
        if (!$confirmButton) {
            throw new \Exception("Confirm delete button not found in the modal.");
        }

        $confirmButton->click();
    }
}