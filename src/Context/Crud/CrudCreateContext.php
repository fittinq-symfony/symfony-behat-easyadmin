<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Crud;

class CrudCreateContext extends CrudContext
{
    /**
     * @When /^the user visits the create page of object (.*)$/
     */
    public function visitCreatePage(string $objectName)
    {
        $crudControllerName = $this->controllerService->getCrudControllerNamespaceByClassname($objectName);
        $this->visitPath("?crudAction=new&crudControllerFqcn=$crudControllerName");
    }

    /**
     * @Given /^submits the new (?:.*)$/
     */
    public function createEntity()
    {
        $this->getSession()->getPage()->find('xpath', "//span[contains(@class, 'action-label') and text() = 'Create']")->click();
    }
}