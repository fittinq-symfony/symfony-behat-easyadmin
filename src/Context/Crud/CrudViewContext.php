<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Crud;

use Behat\Gherkin\Node\PyStringNode;
use Behat\Mink\Element\NodeElement;
use Exception;
use PHPUnit\Framework\Assert;
use stdClass;

class CrudViewContext extends CrudContext
{
    /**
     * @When /^the user visits the overview page of object (.*)$/
     */
    public function visitOverview(string $objectName)
    {
        $crudControllerName = $this->controllerService->getCrudControllerNamespaceByClassname($objectName);
        $this->visitPath("?crudAction=index&crudControllerFqcn=$crudControllerName");
    }

    /**
     * @Then /^the page should show no results found$/
     */
    public function assertEntityNotBeVisibleInTheList()
    {
        Assert::assertEquals('No results found.', $this->getSession()->getPage()->find('xpath', "//tr[contains(@class, 'no-results')]")->getText());
    }

    /**
     * @Then /^the (?:.*) should be visible in the list$/
     * @throws Exception
     */
    public function assertEntityVisibleInTheList(PyStringNode $data)
    {
        $rows = $this->getSession()->getPage()->findAll('xpath', "//tr");
        $match = false;

        foreach($rows as $row) {
            $match = $this->foundMatch(json_decode($data->getRaw()), $row);
            if ($match) {
                break;
            }
        }

        Assert::assertTrue($match);
    }

    private function foundMatch(stdClass $expectedData, NodeElement $row): bool
    {
        $fields = $row->findAll('xpath', "//td");
        $match = false;

        foreach ($fields as $field) {
            foreach ($expectedData as $key => $value) {
                if ($key == $field->getAttribute("data-label")) {
                    if ($value == $field->getText()) {
                        $match = true;
                    }else {
                        return false;
                    }
                }
            }
        }

        return $match;
    }
}