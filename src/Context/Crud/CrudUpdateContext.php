<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Crud;

class CrudUpdateContext extends CrudContext
{
    /**
     * @When /^the user visits the edit page of object (.*) where field (.*) has value (.*)$/
     */
    public function visitEditPage(string $objectName, string $fieldName, string $valueName)
    {
        $crudControllerName = $this->controllerService->getCrudControllerNamespaceByClassname($objectName);
        $id = $this->getEntityId($objectName, $fieldName, $valueName);
        $this->visitPath("?crudAction=edit&crudControllerFqcn=$crudControllerName&entityId=$id");
    }

    /**
     * @When /^the user visits the edit page of object (.*) where relational field (.*) has property (.*) value (.*)$/
     */
    public function visitEditPageByRelation(string $objectName, string $fieldName, string $name, string $value)
    {
        $crudControllerName = $this->controllerService->getCrudControllerNamespaceByClassname($objectName);
        $id = $this->getEntityIdByRelation($objectName, $fieldName, $name, $value);
        $this->visitPath("?crudAction=edit&crudControllerFqcn=$crudControllerName&entityId=$id");
    }

    /**
     * @When /^save changes$/
     */
    public function saveChanges()
    {
        $this->getSession()->getPage()->find('xpath', "//span[contains(@class, 'action-label') and text() = 'Save changes']")->click();
    }
}