<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Crud;

use Behat\MinkExtension\Context\RawMinkContext;
use Doctrine\ORM\EntityManagerInterface;
use Fittinq\Symfony\Behat\Easyadmin\Service\CrudControllerService;

class CrudContext extends RawMinkContext
{
    protected CrudControllerService $controllerService;
    protected EntityManagerInterface $entityManager;

    public function __construct(CrudControllerService $controllerService, EntityManagerInterface $entityManager)
    {
        $this->controllerService = $controllerService;
        $this->entityManager = $entityManager;
    }

    protected function getEntityId(string $objectName, string $fieldName, string $value): int
    {
        $repository = $this->entityManager->getRepository("App\\Entity\\$objectName");
        $entity = $repository->findOneBy([$fieldName => $value]);

        return $entity->getId();
    }

    protected function getEntityIdByRelation(string $objectName, string $fieldName, string $name,  string $value): ?int
    {
        $repository = $this->entityManager->getRepository("App\\Entity\\$objectName");
        $entities = $repository->findAll();

        foreach ($entities as $entity){
            $relation = $entity->{"get".ucfirst($fieldName)}();
            if($relation->{"get".$name}() === $value){
                return $entity->getId();
            }
        }

        return null;
    }
}