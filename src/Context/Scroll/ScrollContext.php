<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin\Context\Scroll;

use Behat\MinkExtension\Context\RawMinkContext;


class ScrollContext extends RawMinkContext
{
    /**
     * @Given /^scrolls (.*) field from object (.*) into view$/
     */
    public function scrollsDown($label, $object)
    {
        $this->getSession()->getPage();
        $this->scrollAndClick($object . "_" . $label);
        sleep(1);
    }

    public function scrollAndClick($cssSelector)
    {
        $scrollIntoView = <<<JS
        (
            function()
            {
                document.querySelector("label[for="+"$cssSelector"+"]").scrollIntoView();
            }
        )() 
        JS;

        $this->getSession()->executeScript($scrollIntoView);
    }
}