<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Easyadmin;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyBehatEasyadminBundle extends Bundle
{
}