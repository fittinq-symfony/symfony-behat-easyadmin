# Symfony Behat Bundle for Testing Easyadmin

The Symfony Behat Bundle for testing Easyadmin, known as `Fittinq\Symfony\Behat\Easyadmin`, is a collection of Behat contexts and services designed to facilitate the testing of Easyadmin-based applications. This bundle provides contexts for creating, updating, deleting, and viewing Easyadmin entities and services to simplify the testing process.

## Table of Contents

- [Introduction](#introduction)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)

## Introduction

Testing Easyadmin-based applications can be challenging, but this Symfony Behat Bundle simplifies the process. It offers a set of Behat contexts that allow you to write feature tests for Easyadmin entities, and services to make testing even more efficient.

## Installation

To include the Symfony Behat Bundle in your project, follow these steps:

1. Require the bundle via Composer:

   ```bash
   composer require fittinq/symfony-behat-easyadmin
   ```

## Usage
The Symfony Behat Bundle for Easyadmin provides contexts for creating, updating, deleting, and viewing Easyadmin
entities. You can use these contexts in your feature tests.

Here's an example of using the CrudCreateContext in a Behat feature file:
```gherkin
    Scenario: carrier should be visible in the list when it is created
        When the user visits the create page of object Carrier
        And set text field Name to value POST-NL
        And submits the new carrier
        Then the carrier should be visible in the list
        """
        {
          "Name" : "POST-NL"
        }
        """
```

This an example of using the CrudUpdateContext in a Behat feature file:
```gherkin
    Scenario: the modified carrier should be visible in the list when it is updated
        When the user visits the edit page of object Carrier where field name has value DHL
        And set text field Name to value POST-NL
        And save changes
        Then the carrier should be visible in the list
        """
            {
              "Name" : "POST-NL"
            }
        """
```

And This an example of using the CrudDeleteContext in a Behat feature file:
```gherkin
    Scenario: queue should not be visible in the list and in RabbitMQ when it is deleted
      Given the user visits the overview page of object Queue
      And the user deletes entity with field Event and value exchange
      Then the page should show no results found
      And there should be no hip.exchange.salesforce queue
```

## Configuration

Update your project to include the Behat bundle in your test setup.
   ```yaml
    default:
       default:
       paths:
          - behat/features
       contexts:
          - Fittinq\Symfony\Behat\Easyadmin\Context\Crud\CrudCreateContext
          - Fittinq\Symfony\Behat\Easyadmin\Context\Crud\CrudUpdateContext
          - Fittinq\Symfony\Behat\Easyadmin\Context\Crud\CrudDeleteContext
          - Fittinq\Symfony\Behat\Easyadmin\Context\Crud\CrudViewContext
   ```